.. _extrinsic-parameters-generation:

Generating Extrinsic Parameter Distributions
============================================

This tutorial will show you how to regenerate the extrinsic parameter
distributions used to determine the likelihood ratio term that accounts for the
relative times-of-arrival, phases, and amplitudes of a CBC signal at each of
the LVK detectors.

There are two parts described below that represents different terms.  Full
documentation can be found here:
https://lscsoft.docs.ligo.org/gstlal/gstlal-inspiral/python-modules/stats.inspiral_extrinsics.html

Setting up the dt, dphi, dsnr dag
^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^

1. Setup a work area and obtain the necessary input files
"""""""""""""""""""""""""""""""""""""""""""""""""""""""""

You will need to create a directory on a cluster running HTCondor, e.g.,

.. code:: bash

    $ mkdir dt_dphi_dsnr
    $ cd dt_dphi_dsnr

This workflow requires estimates of the power spectral densities for LIGO,
Virgo and KAGRA.  For this tutorial we use projected O4 sensitivities in the
LIGO DCC. You can feel free to substitute these to suit your needs.

We will use the following files found at: https://dcc.ligo.org/LIGO-T2000012 ::

    aligo_O4high.txt
    avirgo_O4high_NEW.txt
    kagra_3Mpc.txt

Download the above files and place them in the dt_dphi_dsnr directory that you are currently in.


2.  Excecute commands to generate the HTCondorDAG
"""""""""""""""""""""""""""""""""""""""""""""""""

For this tutorial, we assume that you have a singularity container with the
gstlal software.  More details can be found here:
https://lscsoft.docs.ligo.org/gstlal/installation.html

The following Makefile illustrates the sequence of commands required to generate an HTCondor workflow.  You can copy this into a file called ``Makefile`` and modify it as you wish.

.. code:: make

    SINGULARITY_IMAGE=/ligo/home/ligo.org/chad.hanna/development/gstlal-dev/
    sexec=singularity exec $(SINGULARITY_IMAGE)
    
    all: dt_dphi.dag
    
    # 417.6 Mpc Horizon
    H1_aligo_O4high_psd.xml.gz: aligo_O4high.txt
    	$(sexec) gstlal_psd_xml_from_asd_txt --instrument=H1 --output $@ $<
    
    # 417.6 Mpc Horizon
    L1_aligo_O4high_psd.xml.gz: aligo_O4high.txt
    	$(sexec) gstlal_psd_xml_from_asd_txt --instrument=L1 --output $@ $<
    
    # 265.8 Mpc Horizon
    V1_avirgo_O4high_NEW_psd.xml.gz: avirgo_O4high_NEW.txt
    	$(sexec) gstlal_psd_xml_from_asd_txt --instrument=V1 --output $@ $<
    
    # 6.16 Mpc Horizon
    K1_kagra_3Mpc_psd.xml.gz: kagra_3Mpc.txt
    	$(sexec) gstlal_psd_xml_from_asd_txt --instrument=K1 --output $@ $<
    
    O4_projected_psds.xml.gz: H1_aligo_O4high_psd.xml.gz L1_aligo_O4high_psd.xml.gz V1_avirgo_O4high_NEW_psd.xml.gz K1_kagra_3Mpc_psd.xml.gz
    	$(sexec) ligolw_add --output $@ $^
    
    # SNR ratios according to horizon ratios
    dt_dphi.dag: O4_projected_psds.xml.gz 
    	$(sexec) gstlal_inspiral_create_dt_dphi_snr_ratio_pdfs_dag \
    		--psd-xml $< \
    		--H-snr 8.00 \
    		--L-snr 8.00 \
    		--V-snr 5.09 \
    		--K-snr 0.12 \
    		--m1 1.4 \
    		--m2 1.4 \
    		--s1 0.0 \
    		--s2 0.0 \
    		--flow 15.0 \
    		--fhigh 1024.0 \
    		--NSIDE 16 \
    		--n-inc-angle 33 \
    		--n-pol-angle 33 \
    		--singularity-image $(SINGULARITY_IMAGE)
    
    clean:
    	rm -rf H1_aligo_O4high_psd.xml.gz L1_aligo_O4high_psd.xml.gz V1_avirgo_O4high_NEW_psd.xml.gz logs dt_dphi.dag gstlal_inspiral_compute_dtdphideff_cov_matrix.sub gstlal_inspiral_create_dt_dphi_snr_ratio_pdfs.sub gstlal_inspiral_add_dt_dphi_snr_ratio_pdfs.sub dt_dphi.sh

3.  Submit the HTCondor DAG and monitor the output
""""""""""""""""""""""""""""""""""""""""""""""""""

Next run make to generate the HTCondor DAG

.. code:: bash
    $ make

Then submit the DAG

.. code:: bash
    $ condor_submit_dag dt_dphi.dag

You can check the DAG progress by doing

.. code:: bash
    $ tail -f dt_dphi.dag.dagman.out


4.  Test the output
"""""""""""""""""""

When the DAG completes successfully, you should have a file called ``inspiral_dtdphi_pdf.h5``.  You can verify that this file works with a python terminal, e.g.,

.. code:: bash
    $ singularity exec /ligo/home/ligo.org/chad.hanna/development/gstlal-dev/ python3
    Python 3.6.8 (default, Nov 10 2020, 07:30:01) 
    [GCC 4.8.5 20150623 (Red Hat 4.8.5-44)] on linux
    Type "help", "copyright", "credits" or "license" for more information.
    >>> from gstlal.stats.inspiral_extrinsics import InspiralExtrinsics
    >>> IE = InspiralExtrinsics(filename='inspiral_dtdphi_pdf.h5')
    >>> 
    
Setting up probability of instrument combinations dag
^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^

1. Setup a work area and obtain the necessary input files
"""""""""""""""""""""""""""""""""""""""""""""""""""""""""

You will need to create a directory on a cluster running HTCondor, e.g.,

.. code:: bash

    $ mkdir p_of_instruments
    $ cd p_of_instruments


2.  Excecute commands to generate the HTCondorDAG
"""""""""""""""""""""""""""""""""""""""""""""""""

Below is a sample Makefile that will work if you are using singularity


3.  Submit the HTCondor DAG
"""""""""""""""""""""""""""

.. code:: bash
    $ condor_submit_dag p_of_I_H1K1L1V1.dag 

.. code:: make
    SINGULARITY_IMAGE=/ligo/home/ligo.org/chad.hanna/development/gstlal-dev/
    sexec=singularity exec $(SINGULARITY_IMAGE)
    
    all:
            $(sexec) gstlal_inspiral_create_p_of_ifos_given_horizon_dag --instrument=H1 --instrument=L1 --instrument=V1 --instrument=K1 --singularity-image $(SINGULARITY_IMAGE)
    
    clean:
            rm -rf gstlal_inspiral_add_p_of_ifos_given_horizon.sub  gstlal_inspiral_create_p_of_ifos_given_horizon.sub  logs p_of_I_H1K1L1V1.dag  p_of_I_H1K1L1V1.sh

See Also
^^^^^^^^

* https://arxiv.org/abs/1901.02227
* https://lscsoft.docs.ligo.org/gstlal/gstlal-inspiral/python-modules/stats.inspiral_extrinsics.html
* https://lscsoft.docs.ligo.org/gstlal/gstlal-inspiral/bin/gstlal_inspiral_create_dt_dphi_snr_ratio_pdfs.html
* https://lscsoft.docs.ligo.org/gstlal/gstlal-inspiral/bin/gstlal_inspiral_create_dt_dphi_snr_ratio_pdfs_dag.html
* https://lscsoft.docs.ligo.org/gstlal/gstlal-inspiral/bin/gstlal_inspiral_compute_dtdphideff_cov_matrix.html
 
