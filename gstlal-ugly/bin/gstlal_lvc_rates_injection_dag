#!/usr/bin/env python3
#
#  Copyright (C) 2018 Sarah Caudill
#
#  This program is free software; you can redistribute it and/or modify
#  it under the terms of the GNU General Public License as published by
#  the Free Software Foundation; either version 2 of the License, or
#  (at your option) any later version.
#
#  This program is distributed in the hope that it will be useful,
#  but WITHOUT ANY WARRANTY; without even the implied warranty of
#  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
#  GNU General Public License for more details.
#
#  You should have received a copy of the GNU General Public License
#  along with with program; see the file COPYING. If not, write to the
#  Free Software Foundation, Inc., 59 Temple Place, Suite 330, Boston,
#  MA  02111-1307  USA
#

## @file gstlal_lvc_rates_injection_dag
# The rates injection dag generator.
#
# ### Usage examples
# - Please add some!
#
# ### Command line interface

__author__ = 'Sarah Caudill <sarah.caudill@ligo.org>'

import os

import random

from optparse import OptionParser
from configparser import ConfigParser
import tempfile
from glue import pipeline

from gstlal import injection_generation_dag

def parse_command_line():
        parser = OptionParser(description = __doc__)
        parser.add_option("--tmp-dir", metavar = "directory", help = "Set tmpdir")
        parser.add_option("--tag", metavar = "str", help = "output dag name")
        parser.add_option("--bns-params-ini", metavar = "filename", help = "Set path to ini file with inj params.")
        parser.add_option("--bns-gaussian-params-ini", metavar = "filename", help = "Set path to ini file with inj params.")
        parser.add_option("--nsbh-params-ini", metavar = "filename", help = "Set path to ini file with inj params.")
        parser.add_option("--bbh-params-ini", metavar = "filename", help = "Set path to ini file with inj params.")
        parser.add_option("--nosplit-params-ini", metavar = "filename", help = "Set path to ini file with inj params.")
        parser.add_option("--analysis-ini", metavar = "filename", help = "Set path to ini file with start and end times of each chunk and reference psd files.")
        parser.add_option("--condor-command", action = "append", default = [], metavar = "command=value", help = "set condor commands of the form command=value; can be given multiple times")

        options, filenames = parser.parse_args()
        return options, filenames

try:
        os.mkdir("logs")
except:
        pass

options, filenames = parse_command_line()

ccommands = {}
for o in options.condor_command:
	osplit = o.split("=")
	k = osplit[0]
	v = "=".join(osplit[1:])
	ccommands.update([(k, v)])


dag = injection_generation_dag.DAG("rates_injections_%s" %(options.tag), logpath = options.tmp_dir)
analysis_parser = ConfigParser()
analysis_parser.read(options.analysis_ini)

### BNS Injections
if options.bns_params_ini is not None:	
	bnsinjparser = ConfigParser()
	bnsinjparser.read(options.bns_params_ini)
	bnsInjJob = injection_generation_dag.InjectionJob("gstlal_lvc_rates_injections", tag_base = "bns_rates_injections", condor_commands = ccommands)
	for analysis_section in analysis_parser.sections():
		for inj_section in bnsinjparser.sections():

			node_opts = {}

			node_opts['random-seed'] = random.randint(1, 100000000)

			for name, value in bnsinjparser.items(inj_section):
				node_opts[name] = value
			for name, value in analysis_parser.items(analysis_section):
				node_opts[name] = value


			bnsinjnode = injection_generation_dag.InjectionNode(bnsInjJob, dag, p_node = [], opts = node_opts)
			bnsinjnode.set_priority(99)

### BNS Gaussian Injections
if options.bns_gaussian_params_ini is not None:
	bnsgaussianinjparser = ConfigParser()
	bnsgaussianinjparser.read(options.bns_gaussian_params_ini)
	bnsGaussianInjJob = injection_generation_dag.InjectionJob("gstlal_lvc_rates_injections", tag_base = "bns_gaussian_rates_injections", condor_commands = ccommands)
	for analysis_section in analysis_parser.sections():
		for inj_section in bnsgaussianinjparser.sections():

			node_opts = {}

			node_opts['random-seed'] = random.randint(1, 100000000)

			for name, value in bnsgaussianinjparser.items(inj_section):
				node_opts[name] = value
			for name, value in analysis_parser.items(analysis_section):
				node_opts[name] = value


			bnsgaussianinjnode = injection_generation_dag.InjectionNode(bnsGaussianInjJob, dag, p_node = [], opts = node_opts)
			bnsgaussianinjnode.set_priority(99)


### NSBH Injections
if options.nsbh_params_ini is not None:
	nsbhinjparser = ConfigParser()
	nsbhinjparser.read(options.nsbh_params_ini)
	nsbhInjJob = injection_generation_dag.InjectionJob("gstlal_lvc_rates_injections", tag_base = "nsbh_rates_injections", condor_commands = ccommands)
	for analysis_section in analysis_parser.sections():
		for inj_section in nsbhinjparser.sections():

			node_opts = {}

			node_opts['random-seed'] = random.randint(1, 100000000)

			for name, value in nsbhinjparser.items(inj_section):
				node_opts[name] = value
			for name, value in analysis_parser.items(analysis_section):
				node_opts[name] = value


			nsbhinjnode = injection_generation_dag.InjectionNode(nsbhInjJob, dag, p_node = [], opts = node_opts)
			nsbhinjnode.set_priority(99)


### BBH Injections
if options.bbh_params_ini is not None:
	bbhinjparser = ConfigParser()
	bbhinjparser.read(options.bbh_params_ini)
	bbhInjJob = injection_generation_dag.InjectionJob("gstlal_lvc_rates_injections", tag_base = "bbh_rates_injections", condor_commands = ccommands)
	for analysis_section in analysis_parser.sections():
		for inj_section in bbhinjparser.sections():

			node_opts = {}

			node_opts['random-seed'] = random.randint(1, 100000000)

			for name, value in bbhinjparser.items(inj_section):
				node_opts[name] = value
			for name, value in analysis_parser.items(analysis_section):
				node_opts[name] = value


			bbhinjnode = injection_generation_dag.InjectionNode(bbhInjJob, dag, p_node = [], opts = node_opts)
			bbhinjnode.set_priority(99)

### NOSPLIT Injections
if options.nosplit_params_ini is not None:
	nosplitinjparser = ConfigParser()
	nosplitinjparser.read(options.nosplit_params_ini)
	nosplitInjJob = injection_generation_dag.InjectionJob("gstlal_lvc_rates_injections", tag_base = "nosplit_rates_injections", condor_commands = ccommands)
	for analysis_section in analysis_parser.sections():
		for inj_section in nosplitinjparser.sections():

			node_opts = {}

			node_opts['random-seed'] = random.randint(1, 100000000)

			for name, value in nosplitinjparser.items(inj_section):
				node_opts[name] = value
			for name, value in analysis_parser.items(analysis_section):
				node_opts[name] = value

			nosplitinjnode = injection_generation_dag.InjectionNode(nosplitInjJob, dag, p_node = [], opts = node_opts)
			nosplitinjnode.set_priority(99)

			#all_inj_nodes[(analysis_section, inj_section)].append(inj_nodes)

dag.write_sub_files()
dag.write_dag()
dag.write_script()
