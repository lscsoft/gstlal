# Script for launching PyCharm IDE for python development with all deps

# Discover repository paths (leading underscores to avoid collision with set_env
_SCRIPT_PATH="$(
  cd -- "$(dirname "$0")" >/dev/null 2>&1
  pwd -P
)"
_APPS_PATH=$(
  builtin cd $_SCRIPT_PATH
  pwd
)
_CONDA_PATH=$(
  builtin cd $_APPS_PATH
  cd ..
  pwd
)
_SHARE_PATH=$(
  builtin cd $_CONDA_PATH
  cd ..
  pwd
)

# Determine Operating System
echo "Detecting OS"
if [[ "$OSTYPE" == "linux-gnu"* ]]; then
  SYS_OS="linux"
  ENV_LOCK_ROOT=$CONDA_PATH/envs/qualified-linux-64
elif [[ "$OSTYPE" == "darwin"* ]]; then
  SYS_OS="macos"
  ENV_LOCK_ROOT=$CONDA_PATH/envs/qualified-osx-64
elif [[ "$OSTYPE" == "cygwin" ]]; then
  SYS_OS="cygwin"
  echo "Cygwin currently unsupported"
  exit
elif [[ "$OSTYPE" == "msys" ]]; then
  SYS_OS="win"
  echo "Windows currently unsupported"
  exit
else
  echo "Unsupported OS: $OSTYPE"
  exit
fi
echo "OS detected: $SYS_OS"

# Source env
source $_CONDA_PATH/set_env.sh

# Launch pycharm
echo "Launching PyCharm"
if [[ ($SYS_OS == "macos") ]]; then
  open -na "PyCharm.app"
elif [[ ($SYS_OS == "linux") ]]; then
  ./opt/pycharm/bin/pycharm.sh
fi
