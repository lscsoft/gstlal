# Launch a bash shell

# Discover repository paths
SCRIPT_PATH="$(
  cd -- "$(dirname "$0")" >/dev/null 2>&1
  pwd -P
)"
APPS_PATH=$(
  builtin cd $SCRIPT_PATH
  pwd
)
CONDA_PATH=$(
  builtin cd $APPS_PATH
  cd ..
  pwd
)

source $CONDA_PATH/set_env.sh
