# Copyright (C) 2013-2016 Kipp Cannon
# Copyright (C) 2015      Chad Hanna
# Copyright (C) 2023      Cort Posnansky
#
# This program is free software; you can redistribute it and/or modify it
# under the terms of the GNU General Public License as published by the
# Free Software Foundation; either version 2 of the License, or (at your
# option) any later version.
#
# This program is distributed in the hope that it will be useful, but
# WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU General
# Public License for more details.
#
# You should have received a copy of the GNU General Public License along
# with this program; if not, write to the Free Software Foundation, Inc.,
# 51 Franklin Street, Fifth Floor, Boston, MA  02110-1301, USA.


import os


def set_matplotlib_cache_directory():
	"""
	Change the matplotlib cache directory to a local temporary location.
	"""
	#FIXME A cleaner solution would be to set MPLCONFIGDIR in the condor
	# submit file, but doing so is either nontrivial or unsupported.
	if os.getenv("_CONDOR_SCRATCH_DIR"):
		os.environ['MPLCONFIGDIR'] = os.getenv("_CONDOR_SCRATCH_DIR")

