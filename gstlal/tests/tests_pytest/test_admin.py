"""Unit tests for admin module

"""
import pytest

from gstlal.utilities import admin


class TestDeprecatedDecorator:
    """Tests for deprecation decorator"""

    def test_deprecate_function(self):
        """Test deprecation of function"""

        def sample_func(a, b):
            return a + b

        # ensure calling function before decoration doesn't warn
        with pytest.warns(None) as rec:
            sample_func(1, 2)
        assert len(rec) == 0

        # ensure calling function after decoration does warn
        dec_sample_func = admin.deprecated('Sample Reason')(sample_func)
        with pytest.warns(None) as rec:
            dec_sample_func(1, 2)
        assert len(rec) == 1
        assert isinstance(rec.list[0].message, DeprecationWarning)
        assert rec.list[0].message.args[0] == 'Function sample_func is deprecated: Sample Reason'

    def test_deprecate_class(self):
        """Test deprecation of function"""

        class SampleClass:
            def __init__(self, a, b):
                self.a = a
                self.b = b

            def sample_method(self):
                return self.a + self.b

        # ensure calling function before decoration doesn't warn
        with pytest.warns(None) as rec:
            c = SampleClass(1, 2)
        assert len(rec) == 0

        # ensure calling function after decoration does warn
        DecSampleClass = admin.deprecated('Sample Reason')(SampleClass)
        with pytest.warns(None) as rec:
            DecSampleClass(1, 2)
        assert len(rec) == 1
        assert isinstance(rec.list[0].message, DeprecationWarning)
        assert rec.list[0].message.args[0] == 'Class SampleClass is deprecated: Sample Reason'

    def test_deprecate_method(self):
        """Test deprecation of function"""

        class SampleClass:
            def __init__(self, a, b):
                self.a = a
                self.b = b

            def sample_method(self):
                return self.a + self.b

        # ensure calling function before decoration doesn't warn
        with pytest.warns(None) as rec:
            c = SampleClass(1, 2)
            _ = c.sample_method()
        assert len(rec) == 0

        # ensure calling function after decoration does warn
        SampleClass.sample_method = admin.deprecated('Sample Reason')(SampleClass.sample_method)
        with pytest.warns(None) as rec:
            c.sample_method()
        assert len(rec) == 1
        assert isinstance(rec.list[0].message, DeprecationWarning)
        assert rec.list[0].message.args[0] == 'Function sample_method is deprecated: Sample Reason'
