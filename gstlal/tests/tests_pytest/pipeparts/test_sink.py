"""Unittests for sink pipeparts"""
import pytest

from gstlal import gstpipetools
from gstlal.pipeparts import pipetools, sink
from gstlal.pipeparts.pipetools import Gst, GObject
from gstlal.utilities import testtools


def assert_elem_props(elem, name):
	assert gstpipetools.is_element(elem)
	assert type(elem).__name__ == name


class TestSink:
	"""Group test for encoders"""

	@pytest.fixture(scope='class', autouse=True)
	def pipeline(self):
		GObject.MainLoop()
		return Gst.Pipeline('TestEncode')

	@pytest.fixture(scope='class', autouse=True)
	def src(self, pipeline):
		return pipetools.make_element_with_src(pipeline, None, 'fakesrc')

	def test_multi_file(self, pipeline, src):
		"""Test test multi file"""
		elem = sink.multi_file(pipeline, src)
		assert_elem_props(elem, 'GstMultiFileSink')

	@testtools.requires_full_build
	def test_gwf(self, pipeline, src):
		"""Test test framecpp file"""
		elem = sink.gwf(pipeline, src)
		assert_elem_props(elem, 'FRAMECPPFilesink')

	def test_fake(self, pipeline, src):
		"""Test test fake"""
		elem = sink.fake(pipeline, src)
		assert_elem_props(elem, 'GstFakeSink')

	def test_file(self, pipeline, src):
		"""Test test file"""
		elem = sink.file(pipeline, src, 'file')
		assert_elem_props(elem, 'GstFileSink')

	def test_nxy_dump(self, pipeline, src):
		"""Test nxy dump"""
		elem = sink.tsv(pipeline, src, 'file')
		assert_elem_props(elem, 'GstFileSink')

	@testtools.broken('ogmvideo not available in current version of Gst')
	def test_ogm_video(self, pipeline, src):
		"""Test ogm_video"""
		elem = sink.ogm_video(pipeline, src, 'file')
		assert_elem_props(elem, 'GstFileSink')

	def test_auto_video(self, pipeline, src):
		"""Test auto_video"""
		elem = sink.auto_video(pipeline, src)
		assert_elem_props(elem, 'GstAutoVideoSink')

	def test_auto_audio(self, pipeline, src):
		"""Test auto_audio"""
		elem = sink.auto_audio(pipeline, src)
		assert_elem_props(elem, 'GstAutoAudioSink')

	@testtools.broken('element_link_many not available in current version of Gst')
	def test_playback(self, pipeline, src):
		"""Test playback"""
		elem = sink.playback(pipeline, src)
		assert_elem_props(elem, 'GstAutoAudioSink')

	def test_nxy_dump_tee(self, pipeline, src):
		"""Test nxydumptee"""
		elem = sink.tsv_tee(pipeline, src, 'file')
		assert_elem_props(elem, 'GstTee')

	@testtools.broken('Unable to find implementation of lal_triggerxmlwriter')
	def test_trigger_xml_writer(self, pipeline, src):
		"""Test trigger xml writer"""
		elem = sink.trigger_xml_writer(pipeline, src, 'file')
		assert_elem_props(elem, 'GstAutoAudioSink')

	def test_app(self, pipeline, src):
		"""Test app"""
		elem = sink.app(pipeline, src)
		assert_elem_props(elem, 'GstAppSink')

	@testtools.broken('Implementation bug, invalid property unit_type for tcpserversink element')
	def test_tcp_server(self, pipeline, src):
		"""Test tcp_server"""
		elem = sink.tcp_server(pipeline, src)
		assert_elem_props(elem, 'GstTcpServerSink')
