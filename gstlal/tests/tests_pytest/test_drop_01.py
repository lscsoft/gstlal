"""Unit tests for drop 01

"""

import numpy

from gstlal import pipeparts
from utils import cmp_nxydumps, common


class TestDrop01:
    """Test class wrapper for drop 01"""

    def test_float_32(self):
        """Test 32 bit float drop"""
        drop_test_02("drop_test_02a", "float64", length=13147, drop_samples=1337, sample_fuzz=cmp_nxydumps.default_sample_fuzz)

    def test_float_64(self):
        """Test 64 bit float drop"""
        drop_test_02("drop_test_02a", "float64", length=13147, drop_samples=1337, sample_fuzz=cmp_nxydumps.default_sample_fuzz)


def drop_test_02(name, dtype, length, drop_samples, sample_fuzz=cmp_nxydumps.default_sample_fuzz):
    channels_in = 1
    numpy.random.seed(0)
    # check that the first array is dropped
    input_array = numpy.random.random((length, channels_in)).astype(dtype)
    output_reference = input_array[drop_samples:]
    output_array = numpy.array(common.transform_arrays([input_array], pipeparts.mkdrop, name, drop_samples=drop_samples))
    residual = abs((output_array - output_reference))
    if residual[residual > sample_fuzz].any():
        raise ValueError("incorrect output:  expected %s, got %s\ndifference = %s" % (output_reference, output_array, residual))
